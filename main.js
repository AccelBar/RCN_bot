const { Client, Intents } = require('discord.js');
const { token } = require('./config.json');

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

client.once('ready', () => {
    console.log('0%');
    console.log('25%');
    console.log('50%');
    console.log('75%');
    console.log('100%');
	console.log('Bot is running!');
});

client.login(token);